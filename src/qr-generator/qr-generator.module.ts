import { Module } from '@nestjs/common';
import { QrGeneratorService } from './service/qr-generator.service';
import { QrGeneratorController } from './controller/qr-generator.controller';
import { LeadModule } from 'src/lead/lead.module';

@Module({
  imports: [LeadModule],
  providers: [QrGeneratorService],
  controllers: [QrGeneratorController]
})
export class QrGeneratorModule {}
