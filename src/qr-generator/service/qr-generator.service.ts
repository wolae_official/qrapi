import { Injectable } from '@nestjs/common';
import { Lead } from 'src/lead/entity/lead.entity';
import { LeadService } from 'src/lead/service/lead.service';
import QRCode = require('easyqrcodejs-nodejs')
@Injectable()
export class QrGeneratorService {
    constructor(private leadService: LeadService) {

    }

    public async generateQR(uuid: string): Promise<string> {

        let leadToGenerate: Lead;
        await this.leadService.findOne(uuid).then((lead: Lead) => {
            leadToGenerate = lead;
        })
        const qrcode: QRCode = new QRCode(this.generateOptions(leadToGenerate.uuid));
        qrcode.saveImage({
            path: 'src/qr-generated_images/myQr_1.png' // save path
        });
        return '';
    }

    private generateOptions(uuid: string): any {
        const options = {
            text: uuid,
            width: 256,
            height: 256,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H, // L, M, Q, H
            // logo: "src/wolae_icon_logo.jpeg", 
            //logoWidth: 80, // fixed logo width. default is `width/3.5`
            //logoHeight: 80, // fixed logo height. default is `heigth/3.5`,
            backgroundImage: 'src/wolae_icon_logo.jpeg',
            backgroundImageAlpha: 1, // Background image transparency, value between 0 and 1. default is 1. 
            autoColor: false, // Automatic color adjustment(for data block)
            autoColorDark: "rgba(0, 0, 0, .6)", // Automatic color: dark CSS color
            autoColorLight: "rgba(255, 255, 255, .7)", // Automatic color: light CSS color
      
        }
        return options;
    }

}
