import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { QrGeneratorDto } from '../dto/qr-generator.dto';
import { QrGeneratorService } from '../service/qr-generator.service';

@ApiTags('Qr Generator')
@Controller('qr-generator')
export class QrGeneratorController {
    constructor(private qrGeneratorService: QrGeneratorService) { }
    @Post()
    generateQR(@Body() qr: QrGeneratorDto) {
        return this.qrGeneratorService.generateQR(qr.uuid)
    }
}
