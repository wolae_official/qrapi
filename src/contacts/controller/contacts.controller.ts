import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post, UseFilters } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from 'src/core/filters/http-exception.filter';
import { DeleteResponse } from 'src/core/models/delete-response.model';
import { ContactDto } from '../dto/contact.dto';
import { ContactUdto } from '../dto/contact.udto';
import { Contact } from '../entity/contact.entity';
import { ContactsService } from '../service/contacts.service';

@ApiTags('Contacts')
@Controller('contacts')
@UseFilters(HttpExceptionFilter)
export class ContactsController {
  constructor(private readonly businessService: ContactsService) {}

  @Get()
  findAll(): Promise<Contact[]> {
    return this.businessService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Contact> {
    const businessById = this.businessService.findOne(id);
    return businessById.then(value => {
      if (!value)
        throw new HttpException('No se ha encontrado ningún contacto con ese ID', HttpStatus.NOT_FOUND);
      return businessById;
    })
  }

  @Post()
  create(@Body() contactDto: ContactDto): Promise<Contact> {
    return this.businessService.create(contactDto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateContactDto: ContactUdto): Promise<Contact> {
    return this.businessService.update( updateContactDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResponse> {
    const deletedBusiness = this.businessService.remove(id);
    return deletedBusiness.then(value => {
      if (value.affected === 0 )
        throw new HttpException('No se ha podido borrar el negocio correctamente', HttpStatus.BAD_REQUEST);
      return {
        status: HttpStatus.OK,
        message: `El contacto con Id: ${id} ha sido eliminado correctamente`
      };
    });
  }
}
