import { Lead } from "src/lead/entity/lead.entity";
import { User } from "src/users/entity/user.entity";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne, OneToOne } from "typeorm";

@Entity()
export class Contact {
    @PrimaryGeneratedColumn("uuid")
    uuid: string;

    @ManyToOne(type => User, (user) => user.contacts)
    sender: User

    @OneToMany(type => Lead, (lead) => lead.contact)
    lead: Lead

    @Column()
    name: string;

    @Column()
    phone: string;

    @Column()
    email: string;

    @Column()
    type: string;
  
    @Column({ default: true })
    isActive: boolean;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string
}