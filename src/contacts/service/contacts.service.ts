import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { ContactDto } from '../dto/contact.dto';
import { ContactUdto } from '../dto/contact.udto';
import { Contact } from '../entity/contact.entity';

@Injectable()
export class ContactsService {
    constructor(
        @InjectRepository(Contact)
        private contactRepository: Repository<Contact>
    ) { }

    async findAll(): Promise<Contact[]> {
        return await this.contactRepository.find({relations:['sender', 'lead']});
      }
    
      async findOne(id: string): Promise<Contact> {
        return await this.contactRepository.findOne(id, {relations:['sender', 'lead']});
      }
    
      async create(ContactDto: ContactDto): Promise<Contact> {
        return await this.contactRepository.save(ContactDto);
      }
    
      async update( updateContactDto: ContactUdto): Promise<Contact> {
         return await this.contactRepository.save(updateContactDto);
      }
    
      async remove(id: string): Promise<DeleteResult> {
        return await this.contactRepository.delete(id);
      }
}
