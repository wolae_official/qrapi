import { User } from "src/users/entity/user.entity";

export class ContactUdto {
    uuid: string;

    sender: User

    name: string;

    phone: string;

    email: string;

    type: string;
}