import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entity/user.entity';
import { ContactsController } from './controller/contacts.controller';
import { Contact } from './entity/contact.entity';
import { ContactsService } from './service/contacts.service';

@Module({
  imports: [TypeOrmModule.forFeature([Contact, User])],
  controllers: [ContactsController],
  providers: [ContactsService]
})
export class ContactsModule {}
