import { Test, TestingModule } from '@nestjs/testing';
import { TipoPromocion } from '../entity/promotion-type.entity';

describe('TipoPromocion', () => {
  let service: TipoPromocion;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TipoPromocion],
    }).compile();

    service = module.get<TipoPromocion>(TipoPromocion);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
