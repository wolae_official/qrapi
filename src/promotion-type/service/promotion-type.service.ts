import { PromotionTypeDto } from '../dto/promotion-type.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { PromotionType } from '../entity/promotion-type.entity';

@Injectable()
export class PromotionTypeService {
    constructor(
        @InjectRepository(PromotionType)
        private PromotionTypeRepository: Repository<PromotionType>
    ) { }

    async findAll(): Promise<PromotionType[]> {
        return await this.PromotionTypeRepository.find({relations: ['user']});
    }

    async findOne(uuid: string): Promise<PromotionType> {
        return await this.PromotionTypeRepository.findOne(uuid, {relations: ['user']});
    }

    async create(dto: PromotionTypeDto): Promise<PromotionType> {
        return await this.PromotionTypeRepository.save(dto);
    }

    async update(uuid: string, dto: PromotionTypeDto): Promise<PromotionTypeDto> {
        const promotionType = await this.findOne(uuid);
        const editedPromotionType = Object.assign(promotionType, dto);
        return await this.PromotionTypeRepository.save(editedPromotionType);
    }

    async remove(uuid: string): Promise<DeleteResult> {
        return await this.PromotionTypeRepository.delete(uuid);
    }
}
