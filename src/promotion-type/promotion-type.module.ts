import { PromotionTypeService } from './service/promotion-type.service';
import { PromotionType } from './entity/promotion-type.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PromotionTypeController } from './controller/promotion-type.controller';
import { User } from 'src/users/entity/user.entity';

@Module({
  imports:[TypeOrmModule.forFeature([PromotionType, User])],
  controllers: [PromotionTypeController],
  providers: [PromotionTypeService]
})
export class PromotionTypeModule {}