import { User } from "src/users/entity/user.entity";

export class PromotionTypeDto {
    label: string;
    user: User;
    status: boolean;
}