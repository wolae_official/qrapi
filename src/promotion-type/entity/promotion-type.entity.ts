
import { PromoTemplate } from 'src/promo-template/entity/promo-template.entity';
import { User } from 'src/users/entity/user.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, JoinTable, ManyToMany, ManyToOne, JoinColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class PromotionType {
    @PrimaryGeneratedColumn("uuid")
    uuid: string;

    @Column()
    label: string;

    @ManyToOne(type => User, (user) => user.promotionType)
    @JoinColumn()
    user: User;

    @OneToMany(type => PromoTemplate, (promo) => promo.promotionType)
    promoTemplate: PromoTemplate[]

    @Column({ default: true })
    status: boolean;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string

   
}