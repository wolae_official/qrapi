import { Test, TestingModule } from '@nestjs/testing';
import { TipoPromocion } from '../entity/promotion-type.entity';

describe('TipoPromocion', () => {
  let controller: TipoPromocion;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TipoPromocion],
    }).compile();

    controller = module.get<TipoPromocion>(TipoPromocion);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
