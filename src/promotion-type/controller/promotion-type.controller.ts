import { DeleteResponse } from 'src/core/models/delete-response.model';
import { PromotionTypeDto } from '../dto/promotion-type.dto';
import { PromotionType } from '../entity/promotion-type.entity';
import { PromotionTypeService } from '../service/promotion-type.service';
import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseFilters } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from 'src/core/filters/http-exception.filter';

@ApiTags('PromotionType')
@Controller('promotion-type')
@UseFilters(HttpExceptionFilter)
export class PromotionTypeController {

    constructor(private promotionTypeService: PromotionTypeService) { }
    
    @Get()
    findAllPromotionType(): Promise<PromotionType[]> {
        return this.promotionTypeService.findAll();
    }

    @Get(':uuid')
    findByUuid(@Param('uuid') uuid: string): Promise<PromotionType> {
        const promotionTypeById = this.promotionTypeService.findOne(uuid);
        return promotionTypeById.then(value => {
            if (!value)
                throw new HttpException('No se ha encontrado ningún tipo de promoción con ese ID', HttpStatus.NOT_FOUND);
            return promotionTypeById;
        });
    }

    @Post()
    create(@Body() dto: PromotionTypeDto) {
        return this.promotionTypeService.create(dto);
    }

    @Put(':uuid')
    update(@Param('uuid') uuid: string, @Body() dto: PromotionTypeDto) {
        return this.promotionTypeService.update(uuid, dto);
    }

    @Delete(':uuid') 
    delete(@Param('uuid') uuid: string): Promise<DeleteResponse> {
        const deletedPromotionType = this.promotionTypeService.remove(uuid);
        return deletedPromotionType.then(value => {
            if (value.affected === 0 )
                throw new HttpException('No se ha podido borrar el lead correctamente', HttpStatus.BAD_REQUEST);

            return {
                status: HttpStatus.OK,
                message: `El lead con Id: ${uuid} ha sido eliminado correctamente`
            };
        });
    }
}
