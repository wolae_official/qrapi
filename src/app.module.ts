import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';

// Modules
import { PromoTemplateModule } from './promo-template/promo-template.module';
import { PromotionTypeModule } from './promotion-type/promotion-type.module';
import { UsersModule } from './users/users.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { BusinessModule } from './business/business.module';
import { LeadModule } from './lead/lead.module';
import { QrGeneratorModule } from './qr-generator/qr-generator.module';
import { ContactsModule } from './contacts/contacts.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    PromoTemplateModule,
    PromotionTypeModule,
    UsersModule,
    AuthenticationModule,
    BusinessModule,
    LeadModule,
    QrGeneratorModule,
    ContactsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
