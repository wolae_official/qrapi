import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LeadController } from './controller/lead.controller';
import { Lead } from './entity/lead.entity';
import { LeadService } from './service/lead.service';

@Module({
  imports:[TypeOrmModule.forFeature([Lead])],
  controllers: [LeadController],
  providers: [LeadService],
  exports:[LeadService]
})
export class LeadModule {}
