import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contact } from 'src/contacts/entity/contact.entity';
import { Repository, DeleteResult } from 'typeorm';
import { LeadDto } from '../dto/lead.dto';
import { Lead } from '../entity/lead.entity';

@Injectable()
export class LeadService {
    constructor(
        @InjectRepository(Lead)
        private leadRepository: Repository<Lead>,
        @InjectRepository(Lead)
        private contactRepository: Repository<Contact>,
      ) { }
    
      async findAll(): Promise<Lead[]> {
        return await this.leadRepository.find(
          {relations: ['contact', 'promotemplate', 'sender']}
        );
      }
    
      async findOne(uuid: string): Promise<Lead> {
        return await this.leadRepository.findOne(uuid,  {relations: ['contact', 'promotemplate', 'sender']});
      }
    
      async create(dto: LeadDto): Promise<Lead> {
        this.contactRepository.save(dto.contact)
        return await this.leadRepository.save(dto);
      }
    
      async update(uuid: string, dto: LeadDto): Promise<Lead> {
        const qr = await this.findOne(uuid);
        const editedQr = Object.assign(qr, dto);
        return await this.leadRepository.save(editedQr);
      }
    
      async remove(uuid: string): Promise<DeleteResult> {
        return await this.leadRepository.delete(uuid);
      }
}
