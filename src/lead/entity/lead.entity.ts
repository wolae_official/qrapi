
import { Contact } from 'src/contacts/entity/contact.entity';
import { PromoTemplate } from 'src/promo-template/entity/promo-template.entity';
import { User } from 'src/users/entity/user.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, ManyToOne, CreateDateColumn, UpdateDateColumn, JoinTable, OneToMany, JoinColumn } from 'typeorm';

@Entity()
export class Lead {
    @PrimaryGeneratedColumn("uuid")
    uuid: string;

    @ManyToOne (type => User, (user) => user.lead)
    sender: User;

    @ManyToOne(type => Contact, (contact) => contact.lead,{ cascade: ['insert', 'update'] })
    contact: Contact;
  
    @OneToOne(type => PromoTemplate, (ptemplate) => ptemplate.lead)
    @JoinColumn()
    promotemplate: PromoTemplate;

    @Column()
    uses_availables: number;
  
    @Column({ default: true })
    isActive: boolean;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string
}