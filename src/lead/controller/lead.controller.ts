import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseFilters } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from 'src/core/filters/http-exception.filter';
import { DeleteResponse } from 'src/core/models/delete-response.model';
import { LeadDto } from '../dto/lead.dto';
import { Lead } from '../entity/lead.entity';
import { LeadService } from '../service/lead.service';
@ApiTags('Leads')
@Controller('lead')
@UseFilters(HttpExceptionFilter)
export class LeadController {
    constructor(private leadService: LeadService){}
    
    @Get()
    findAll(): Promise<Lead[]> {
        return this.leadService.findAll();
    }

    @Get(':uuid')
    findByUuid(@Param('uuid') uuid:string): Promise<Lead> {
        const lead = this.leadService.findOne(uuid);
        return lead.then(value => {
            if (!value)
                throw new HttpException('No se ha encontrado ningún lead con ese ID', HttpStatus.NOT_FOUND);
            return lead;
        });
    }

    @Post()
    create(@Body() dto: LeadDto): Promise<Lead> {
        return this.leadService.create(dto);
    }

    @Put()
    update(@Param('uuid') uuid: string, @Body() dto: LeadDto): Promise<Lead> {
        return this.leadService.update(uuid, dto);
    }

    @Delete(':uuid') 
    delete(@Param('uuid') uuid:string): Promise<DeleteResponse> {
        const deletedLead = this.leadService.remove(uuid);
        return deletedLead.then(value => {
            if (value.affected === 0 )
                throw new HttpException('No se ha podido borrar el lead correctamente', HttpStatus.BAD_REQUEST);
            return {
                status: HttpStatus.OK,
                message: `El lead con Id: ${uuid} ha sido eliminado correctamente`
            };
        });
    }
}
