import { HttpStatus } from '@nestjs/common';
export class DeleteResponse {
    status: HttpStatus;
    message: string;
}