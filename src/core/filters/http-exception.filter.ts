import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception.getStatus();
        const responseMessage = (type: string, message: string) => {
            response
                .status(status)
                .json({
                    statusCode: status,
                    timestamp: new Date().toUTCString(),
                    path: request.url,
                    errorType: type,
                    errorMessage: message
                });
        };
        responseMessage(exception.name, exception.message);
    }
}