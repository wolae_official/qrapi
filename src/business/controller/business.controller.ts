import { DeleteResponse } from 'src/core/models/delete-response.model';
import { Controller, Get, Post, Body, Patch, Param, Delete, UseFilters, HttpException, HttpStatus } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from 'src/core/filters/http-exception.filter';
import { BusinessService } from '../business.service';
import { BusinessDto } from '../dto/business.dto';
import { Business } from '../entity/business.entity';

@ApiTags('Business')
@Controller('business')
@UseFilters(HttpExceptionFilter)
export class BusinessController {
  constructor(private readonly businessService: BusinessService) {}

  @Get()
  findAll(): Promise<Business[]> {
    return this.businessService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Business> {
    const businessById = this.businessService.findOne(id);
    return businessById.then(value => {
      if (!value)
        throw new HttpException('No se ha encontrado ningún negocio con ese ID', HttpStatus.NOT_FOUND);
      return businessById;
    })
  }

  @Post()
  create(@Body() businessDto: BusinessDto): Promise<Business> {
    return this.businessService.create(businessDto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBusinessDto: BusinessDto): Promise<Business> {
    return this.businessService.update(id, updateBusinessDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResponse> {
    const deletedBusiness = this.businessService.remove(id);
    return deletedBusiness.then(value => {
      if (value.affected === 0 )
        throw new HttpException('No se ha podido borrar el negocio correctamente', HttpStatus.BAD_REQUEST);
      return {
        status: HttpStatus.OK,
        message: `El negocio con Id: ${id} ha sido eliminado correctamente`
      };
    });
  }
}
