import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { BusinessService } from './business.service';
import { BusinessController } from './controller/business.controller';
import { Business } from './entity/business.entity';
import { User } from 'src/users/entity/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Business, User])],
  controllers: [BusinessController],
  providers: [BusinessService]
})
export class BusinessModule {}
