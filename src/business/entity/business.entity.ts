import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { User } from 'src/users/entity/user.entity';

@Entity()
export class Business {
    @PrimaryGeneratedColumn("uuid")
    uuid: string;

    @Column()
    name: string;

    @Column({ default: '' })
    location: string;

    @Column({ default: '' })
    image: string;

    @Column({ default: true })
    status: boolean;

    @OneToMany(() => User, (user) => user.business,{ cascade: ['insert', 'update'] })
    employees: User[];

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string
}
