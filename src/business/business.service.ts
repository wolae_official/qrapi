import { Business } from './entity/business.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { BusinessDto } from './dto/business.dto';

@Injectable()
export class BusinessService {
  constructor(
    @InjectRepository(Business)
    private businessRepository: Repository<Business>
) { }

  async findAll(): Promise<Business[]> {
    return await this.businessRepository.find({relations:['employees']});
  }

  async findOne(id: string): Promise<Business> {
    return await this.businessRepository.findOne(id, {relations:['employees']});
  }

  async create(businessDto: BusinessDto): Promise<Business> {
    return await this.businessRepository.save(businessDto);
  }

  async update(id: string, updateBusinessDto: BusinessDto): Promise<Business> {
    const business = await this.findOne(id);
    const editedBusiness = Object.assign(business, updateBusinessDto);
    return await this.businessRepository.save(editedBusiness);
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.businessRepository.delete(id);
  }
}
