import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserRole } from "src/core/enums/user-role.enum";
import { DeleteResult, Repository } from "typeorm";
import { CreateUserDto, RegisterUserDto } from "../dto/users.dto";
import { User } from "../entity/user.entity";
import * as crypto from "crypto";
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  async findAll(): Promise<User[]> {
    const userCollection = await this.userRepository.find(
      {
          relations: ['business', 'promotionType', 'promoTemplates']
      }
    );
    return userCollection;
  }

  async findByEmail(email: string): Promise<User> {
    const userByEmail = await this.userRepository.findOne({
      where: { email },
        relations: ['business', 'promotionType', 'promoTemplates']
    });
    return userByEmail;
  }

  async findById(id: string): Promise<User> {
    const userById = await this.userRepository.findOne({
      where: { uuid: id },
      relations: ['business', 'promotionType', 'promoTemplates']
    });
    return userById;
  }

  async findByBusiness(businessId: string): Promise<User[]> {
    const userCollectionByBusiness = await this.userRepository.find({
      where: {
        business: businessId
      }
    });
    if (userCollectionByBusiness.length === 0)
      return [];
    return userCollectionByBusiness;
  }

  async findByRole(role: UserRole): Promise<User[]> {
    const userCollectionByRole = await this.userRepository.find({
      where: { role }
    });
    return userCollectionByRole;
  }

  async create(user: RegisterUserDto): Promise<User> {
    user.password = crypto.createHmac("sha256", user.password).digest("hex");
    return await this.userRepository.save(user);
  }

  async update(uuid: string, userDto: CreateUserDto): Promise<CreateUserDto> {
    const user = await this.findById(uuid);
    const editedUser = Object.assign(user, userDto);
    return await this.userRepository.create(editedUser);
  }

  async delete(id: string): Promise<DeleteResult> {
    return await this.userRepository.delete(id);
  }
}
