import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, ManyToOne, JoinTable, JoinColumn, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import * as crypto from "crypto";
import { Business } from "src/business/entity/business.entity";
import { UserRole } from "src/core/enums/user-role.enum";
import { Lead } from "src/lead/entity/lead.entity";
import { PromotionType } from "src/promotion-type/entity/promotion-type.entity";
import { PromoTemplate } from "src/promo-template/entity/promo-template.entity";
import { Contact } from "src/contacts/entity/contact.entity";


@Entity()
export class User {
  @PrimaryGeneratedColumn("uuid")
  uuid: string;

  @Column()
  name: string;

  @Column({ default: "" })
  avatar: string;

  @Column({ unique: true })
  email: string;

  @BeforeInsert()
  hashPassword() {
    this.password = crypto.createHmac("sha256", this.password).digest("hex");
  }
  @Column()
  password: string;

  @ManyToOne(type => Business, (businees) => businees.employees, { cascade: ['insert', 'update'] })
  business: Business;

  @OneToMany(type => PromotionType, (promo) => promo.user, { cascade: ['insert', 'update'] })
  @JoinColumn()
  promotionType: PromotionType[];

  @OneToMany(type => PromoTemplate, (promo) => promo.user, { cascade: ['insert', 'update'] })
  @JoinColumn()
  promoTemplates: PromoTemplate[];

  @OneToMany(type => Lead, (lead) => lead.sender, { cascade: ['insert', 'update'] })
  lead: Lead[];

  @OneToMany(type => Contact, (contact) => contact.sender, { cascade: ['insert', 'update'] })
  contacts: Contact[];

  @Column({ type: 'enum', enum: UserRole, default: UserRole.User })
  role: UserRole;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string
}
