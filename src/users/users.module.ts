import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Business } from "src/business/entity/business.entity";
import { PromoTemplate } from "src/promo-template/entity/promo-template.entity";
import { PromotionType } from "src/promotion-type/entity/promotion-type.entity";
import { UsersController } from "./controller/users.controller";
import { User } from "./entity/user.entity";
import { UsersService } from "./service/users.service";

@Module({
  imports: [TypeOrmModule.forFeature([User, Business, PromotionType, PromoTemplate])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
