import { DeleteResponse } from 'src/core/models/delete-response.model';
import { CreateUserDto } from './../dto/users.dto';
import { User } from './../entity/user.entity';
import { UsersService } from './../service/users.service';
import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseFilters } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserRole } from 'src/core/enums/user-role.enum';
import { HttpExceptionFilter } from 'src/core/filters/http-exception.filter';

@ApiTags('Users')
@Controller('users')
@UseFilters(HttpExceptionFilter)
export class UsersController {
    constructor(private usersService: UsersService) { }

    @Get()
    findAllUsers(): Promise<User[]> {
        return this.usersService.findAll();
    }
    
    @Get('/email/:email')
    findByEmail(@Param('email') email: string): Promise<User> {
        const userByemail = this.usersService.findByEmail(email);
        return userByemail.then(value => {
            if (!value)
                throw new HttpException('No se ha encontrado ningún usuario con ese email', HttpStatus.NOT_FOUND);
        return userByemail;
        });
    }

    @Get(':id')
    findById(@Param('id') id: string): Promise<User> {
        const userById = this.usersService.findById(id);
        return userById.then(value => {
            if (!value) 
                throw new HttpException('No se ha encontrado ningún usuario con ese ID', HttpStatus.NOT_FOUND);
            return userById;
        });
    }

    @Get('/business/:business')
    findByBusiness(@Param('business') businessId: string): Promise<User[]> {
        const userByBusiness = this.usersService.findByBusiness(businessId);
        return userByBusiness.then(value => {
            if (!value)
                throw new HttpException('No se ha encontrado ningún usuario asociado al negocio indicado', HttpStatus.NOT_FOUND);
            return userByBusiness;
        });
    }

    @Get('/role/:role')
    findByRole(@Param('role') role: UserRole): Promise<User[]> {
        const userByRole = this.usersService.findByRole(role);
        return userByRole.then(value => {
            if (!value)
                throw new HttpException('No se ha encontrado ningún usuario con el rol indicado', HttpStatus.NOT_FOUND);
            return userByRole;
        });
    }

    @Post()
    create(@Body() dto: CreateUserDto) {
        return this.usersService.create(dto);
    }

    @Put(':id')
    update(@Param('id') id: string, @Body() dto: CreateUserDto) {
        return this.usersService.update(id, dto);
    }

    @Delete(':id') 
    delete(@Param('id') id: string): Promise<DeleteResponse> {
        const deletedUser = this.usersService.delete(id);
        return deletedUser.then(value => {
            if (value.affected === 0 )
                throw new HttpException('No se ha podido borrar el lead correctamente', HttpStatus.BAD_REQUEST);
            return {
                status: HttpStatus.OK,
                message: `El lead con Id: ${id} ha sido eliminado correctamente`
            };
        });
    }
}
