import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PromoTemplateController } from './controller/promo-template.controller';
import { PromoTemplate } from './entity/promo-template.entity';
import { PromoTemplateService } from './service/promo-template.service';

@Module({
  imports:[TypeOrmModule.forFeature([PromoTemplate])],
  controllers: [PromoTemplateController],
  providers: [PromoTemplateService]
})
export class PromoTemplateModule {}
