
import { Lead } from 'src/lead/entity/lead.entity';
import { PromotionType } from 'src/promotion-type/entity/promotion-type.entity';
import { User } from 'src/users/entity/user.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, ManyToOne, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';

@Entity()
export class PromoTemplate {
    @PrimaryGeneratedColumn("uuid")
    uuid: string;

    @ManyToOne(type => User, (user) => user.promoTemplates)
    user: User;
  
    @ManyToOne(type => PromotionType, (promoType) => promoType.promoTemplate)
    promotionType: PromotionType;

    @OneToOne(type => Lead, (lead) => lead.promotemplate)
    lead: Lead

    @Column()
    title: string;

    @Column()
    description: string;
  
    @Column()
    expires: string;

  

    @Column()
    uses: number;

    @Column()
    availability: number;
  
    @Column({ default: true })
    isActive: boolean;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string
}