import { Test, TestingModule } from '@nestjs/testing';
import { PromoTemplateService } from './promo-template.service';

describe('PromoTemplateService', () => {
  let service: PromoTemplateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PromoTemplateService],
    }).compile();

    service = module.get<PromoTemplateService>(PromoTemplateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
