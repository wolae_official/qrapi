import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { PromoTemplateDto } from '../dto/promo-template.dto';
import { PromoTemplate } from '../entity/promo-template.entity';


@Injectable()
export class PromoTemplateService {
  constructor(
    @InjectRepository(PromoTemplate)
    private promoTemplateRepository: Repository<PromoTemplate>,
  ) { }

  async findAll(): Promise<PromoTemplate[]> {
    return await this.promoTemplateRepository.find(
      {  relations: ['user', 'promotionType','lead']}
    );
  }

  async findOne(uuid: string): Promise<PromoTemplate> {
    return await this.promoTemplateRepository.findOne(uuid,  {  relations: ['user', 'promotionType', 'lead']});
  }

  async create(dto: PromoTemplateDto): Promise<PromoTemplate> {
    return await this.promoTemplateRepository.save(dto);
  }

  async update(uuid: string, dto: PromoTemplateDto): Promise<PromoTemplate> {
    const qr = await this.findOne(uuid);
    const editedQr = Object.assign(qr, dto);
    return await this.promoTemplateRepository.save(editedQr);
  }

  async remove(uuid: string): Promise<DeleteResult> {
    return await this.promoTemplateRepository.delete(uuid);
  }
}
