import { DeleteResponse } from 'src/core/models/delete-response.model';
import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseFilters } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PromoTemplateDto } from '../dto/promo-template.dto';
import { PromoTemplate } from '../entity/promo-template.entity';
import { PromoTemplateService } from '../service/promo-template.service';
import { HttpExceptionFilter } from 'src/core/filters/http-exception.filter';

@ApiTags('Promo Template')
@Controller('promotemplate')
@UseFilters(HttpExceptionFilter)
export class PromoTemplateController {
    constructor(private ptService: PromoTemplateService) { }
    
    @Get()
    findAll(): Promise<PromoTemplate[]> {
        return this.ptService.findAll();
    }

    @Get(':uuid')
    findByUuid(@Param('uuid') uuid: string):Promise<PromoTemplate> {
        const ptById = this.ptService.findOne(uuid);
        return ptById.then(value => {
            if (!value)
                throw new HttpException('No se ha encontrado ninguna promo template con ese ID', HttpStatus.NOT_FOUND);
            return ptById;
        });
    }

    @Post()
    create(@Body() dto: PromoTemplateDto): Promise<PromoTemplate> {
        return this.ptService.create(dto);
    }

    @Put()
    update(@Param('uuid') uuid: string, @Body() dto: PromoTemplateDto): Promise<PromoTemplate> {
        return this.ptService.update(uuid, dto);
    }

    @Delete(':uuid') 
    delete(@Param('uuid') uuid: string): Promise<DeleteResponse> {
        const deletedPromoTemplate = this.ptService.remove(uuid);
        return deletedPromoTemplate.then(value => {
            if (value.affected === 0 )
                throw new HttpException('No se ha podido borrar el promo template correctamente', HttpStatus.BAD_REQUEST);
            return {
                status: HttpStatus.OK,
                message: `El promo template con Id: ${uuid} ha sido eliminado correctamente`
            };
        });
    }
}
