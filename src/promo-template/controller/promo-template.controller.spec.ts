import { Test, TestingModule } from '@nestjs/testing';
import { PromoTemplateController } from './promo-template.controller';

describe('PromoTemplateController', () => {
  let controller: PromoTemplateController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PromoTemplateController],
    }).compile();

    controller = module.get<PromoTemplateController>(PromoTemplateController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
