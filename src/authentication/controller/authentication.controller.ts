import { UsersService } from 'src/users/service/users.service';
import { Body, Controller, HttpException, HttpStatus, Post, UseFilters } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { RegisterUserDto } from "src/users/dto/users.dto";
import LoginDto from "../dto/login.dto";
import { AuthenticationService } from "../service/authentication.service";
import { User } from './../../users/entity/user.entity';
import { HttpExceptionFilter } from 'src/core/filters/http-exception.filter';
import { AccessToken } from 'src/core/models/access-token.model';

@ApiTags('Auth')
@Controller("authentication")
@UseFilters(HttpExceptionFilter)
export class AuthenticationController {
  constructor(private readonly authService: AuthenticationService,
              private usersService: UsersService) {}

  @Post("login")
  login(@Body() user: LoginDto): Promise<AccessToken | {status: number}> {
    const login = this.authService.login(user);
    return login.then(value => {
      if (value.status === 404)
        throw new HttpException('Las credenciales introducidas no son correctas', HttpStatus.NOT_FOUND);
      return login;
    });
  }

  @Post("register")
  register(@Body() user: RegisterUserDto): Promise<User> {
    const existingUser = this.usersService.findByEmail(user.email);
    return existingUser.then(value => {
      if (value)
        throw new HttpException('Ya existe un usuario con ese email', HttpStatus.BAD_REQUEST);
      return this.authService.register(user);
    });
  }
}
