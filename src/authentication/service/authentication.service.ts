import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { RegisterUserDto } from "src/users/dto/users.dto";
import { User } from "src/users/entity/user.entity";
import { UsersService } from "src/users/service/users.service";
import LoginDto from "../dto/login.dto";
import * as crypto from "crypto";
import { AccessToken } from 'src/core/models/access-token.model';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  private async validate(userData: LoginDto): Promise<User> {
    return await this.userService.findByEmail(userData.email);
  }

  public async login(user: LoginDto): Promise<AccessToken | { status: number }> {
    const hashedPassword = crypto
        .createHmac("sha256", user.password)
        .digest("hex");
        
    return await this.validate(user).then((userData) => {
      if (!userData || hashedPassword !== userData.password) {
        return { status: 404 };
      }
      const payload = {
        id: `${userData.uuid}`,
        exp: this.setExpireDate(),
      },
      accessToken = this.jwtService.sign(payload);

      return {
        expires_in: 3600,
        access_token: accessToken,
        payload,
        status: 200,
      };
    });
  }

  public async register(user: RegisterUserDto): Promise<User> {

    return await this.userService.create(user);
  }

  private setExpireDate(): number {
    const creationDate: Date = new Date();
    return creationDate.setHours(creationDate.getHours() + 1);
  }
}
