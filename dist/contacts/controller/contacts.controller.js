"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactsController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const http_exception_filter_1 = require("../../core/filters/http-exception.filter");
const delete_response_model_1 = require("../../core/models/delete-response.model");
const contact_dto_1 = require("../dto/contact.dto");
const contact_udto_1 = require("../dto/contact.udto");
const contacts_service_1 = require("../service/contacts.service");
let ContactsController = class ContactsController {
    constructor(businessService) {
        this.businessService = businessService;
    }
    findAll() {
        return this.businessService.findAll();
    }
    findOne(id) {
        const businessById = this.businessService.findOne(id);
        return businessById.then(value => {
            if (!value)
                throw new common_1.HttpException('No se ha encontrado ningún contacto con ese ID', common_1.HttpStatus.NOT_FOUND);
            return businessById;
        });
    }
    create(contactDto) {
        return this.businessService.create(contactDto);
    }
    update(id, updateContactDto) {
        return this.businessService.update(updateContactDto);
    }
    remove(id) {
        const deletedBusiness = this.businessService.remove(id);
        return deletedBusiness.then(value => {
            if (value.affected === 0)
                throw new common_1.HttpException('No se ha podido borrar el negocio correctamente', common_1.HttpStatus.BAD_REQUEST);
            return {
                status: common_1.HttpStatus.OK,
                message: `El contacto con Id: ${id} ha sido eliminado correctamente`
            };
        });
    }
};
__decorate([
    (0, common_1.Get)(),
    openapi.ApiResponse({ status: 200, type: [require("../entity/contact.entity").Contact] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ContactsController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    openapi.ApiResponse({ status: 200, type: require("../entity/contact.entity").Contact }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ContactsController.prototype, "findOne", null);
__decorate([
    (0, common_1.Post)(),
    openapi.ApiResponse({ status: 201, type: require("../entity/contact.entity").Contact }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [contact_dto_1.ContactDto]),
    __metadata("design:returntype", Promise)
], ContactsController.prototype, "create", null);
__decorate([
    (0, common_1.Patch)(':id'),
    openapi.ApiResponse({ status: 200, type: require("../entity/contact.entity").Contact }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, contact_udto_1.ContactUdto]),
    __metadata("design:returntype", Promise)
], ContactsController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    openapi.ApiResponse({ status: 200, type: require("../../core/models/delete-response.model").DeleteResponse }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ContactsController.prototype, "remove", null);
ContactsController = __decorate([
    (0, swagger_1.ApiTags)('Contacts'),
    (0, common_1.Controller)('contacts'),
    (0, common_1.UseFilters)(http_exception_filter_1.HttpExceptionFilter),
    __metadata("design:paramtypes", [contacts_service_1.ContactsService])
], ContactsController);
exports.ContactsController = ContactsController;
//# sourceMappingURL=contacts.controller.js.map