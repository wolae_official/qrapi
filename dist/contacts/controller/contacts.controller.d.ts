import { DeleteResponse } from 'src/core/models/delete-response.model';
import { ContactDto } from '../dto/contact.dto';
import { ContactUdto } from '../dto/contact.udto';
import { Contact } from '../entity/contact.entity';
import { ContactsService } from '../service/contacts.service';
export declare class ContactsController {
    private readonly businessService;
    constructor(businessService: ContactsService);
    findAll(): Promise<Contact[]>;
    findOne(id: string): Promise<Contact>;
    create(contactDto: ContactDto): Promise<Contact>;
    update(id: string, updateContactDto: ContactUdto): Promise<Contact>;
    remove(id: string): Promise<DeleteResponse>;
}
