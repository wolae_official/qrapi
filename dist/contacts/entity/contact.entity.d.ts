import { Lead } from "src/lead/entity/lead.entity";
import { User } from "src/users/entity/user.entity";
export declare class Contact {
    uuid: string;
    sender: User;
    lead: Lead;
    name: string;
    phone: string;
    email: string;
    type: string;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}
