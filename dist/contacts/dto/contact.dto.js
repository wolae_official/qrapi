"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactDto = void 0;
const openapi = require("@nestjs/swagger");
const user_entity_1 = require("../../users/entity/user.entity");
class ContactDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { sender: { required: true, type: () => require("../../users/entity/user.entity").User }, name: { required: true, type: () => String }, phone: { required: true, type: () => String }, email: { required: true, type: () => String }, type: { required: true, type: () => String } };
    }
}
exports.ContactDto = ContactDto;
//# sourceMappingURL=contact.dto.js.map