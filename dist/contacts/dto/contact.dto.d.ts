import { User } from "src/users/entity/user.entity";
export declare class ContactDto {
    sender: User;
    name: string;
    phone: string;
    email: string;
    type: string;
}
