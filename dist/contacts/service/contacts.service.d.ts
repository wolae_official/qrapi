import { DeleteResult, Repository } from 'typeorm';
import { ContactDto } from '../dto/contact.dto';
import { ContactUdto } from '../dto/contact.udto';
import { Contact } from '../entity/contact.entity';
export declare class ContactsService {
    private contactRepository;
    constructor(contactRepository: Repository<Contact>);
    findAll(): Promise<Contact[]>;
    findOne(id: string): Promise<Contact>;
    create(ContactDto: ContactDto): Promise<Contact>;
    update(updateContactDto: ContactUdto): Promise<Contact>;
    remove(id: string): Promise<DeleteResult>;
}
