"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_role_enum_1 = require("../../core/enums/user-role.enum");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../entity/user.entity");
const crypto = __importStar(require("crypto"));
let UsersService = class UsersService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async findAll() {
        const userCollection = await this.userRepository.find({
            relations: ['business', 'promotionType', 'promoTemplates']
        });
        return userCollection;
    }
    async findByEmail(email) {
        const userByEmail = await this.userRepository.findOne({
            where: { email },
            relations: ['business', 'promotionType', 'promoTemplates']
        });
        return userByEmail;
    }
    async findById(id) {
        const userById = await this.userRepository.findOne({
            where: { uuid: id },
            relations: ['business', 'promotionType', 'promoTemplates']
        });
        return userById;
    }
    async findByBusiness(businessId) {
        const userCollectionByBusiness = await this.userRepository.find({
            where: {
                business: businessId
            }
        });
        if (userCollectionByBusiness.length === 0)
            return [];
        return userCollectionByBusiness;
    }
    async findByRole(role) {
        const userCollectionByRole = await this.userRepository.find({
            where: { role }
        });
        return userCollectionByRole;
    }
    async create(user) {
        user.password = crypto.createHmac("sha256", user.password).digest("hex");
        return await this.userRepository.save(user);
    }
    async update(uuid, userDto) {
        const user = await this.findById(uuid);
        const editedUser = Object.assign(user, userDto);
        return await this.userRepository.create(editedUser);
    }
    async delete(id) {
        return await this.userRepository.delete(id);
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map