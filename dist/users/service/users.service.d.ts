import { UserRole } from "src/core/enums/user-role.enum";
import { DeleteResult, Repository } from "typeorm";
import { CreateUserDto, RegisterUserDto } from "../dto/users.dto";
import { User } from "../entity/user.entity";
export declare class UsersService {
    private userRepository;
    constructor(userRepository: Repository<User>);
    findAll(): Promise<User[]>;
    findByEmail(email: string): Promise<User>;
    findById(id: string): Promise<User>;
    findByBusiness(businessId: string): Promise<User[]>;
    findByRole(role: UserRole): Promise<User[]>;
    create(user: RegisterUserDto): Promise<User>;
    update(uuid: string, userDto: CreateUserDto): Promise<CreateUserDto>;
    delete(id: string): Promise<DeleteResult>;
}
