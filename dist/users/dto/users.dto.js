"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterUserDto = exports.CreateUserDto = void 0;
const openapi = require("@nestjs/swagger");
const user_role_enum_1 = require("../../core/enums/user-role.enum");
class CreateUserDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { email: { required: true, type: () => String }, name: { required: true, type: () => String }, password: { required: true, type: () => String }, avatar: { required: true, type: () => String }, business: { required: false, type: () => require("../../business/entity/business.entity").Business }, role: { required: true, enum: require("../../core/enums/user-role.enum").UserRole } };
    }
}
exports.CreateUserDto = CreateUserDto;
class RegisterUserDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { name: { required: true, type: () => String }, email: { required: true, type: () => String }, password: { required: true, type: () => String } };
    }
}
exports.RegisterUserDto = RegisterUserDto;
exports.default = CreateUserDto;
//# sourceMappingURL=users.dto.js.map