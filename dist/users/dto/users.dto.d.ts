import { UserRole } from 'src/core/enums/user-role.enum';
import { Business } from './../../business/entity/business.entity';
export declare class CreateUserDto {
    email: string;
    name: string;
    password: string;
    avatar: string;
    business?: Business;
    role: UserRole;
}
export declare class RegisterUserDto {
    name: string;
    email: string;
    password: string;
}
export default CreateUserDto;
