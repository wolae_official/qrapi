import { Business } from "src/business/entity/business.entity";
import { UserRole } from "src/core/enums/user-role.enum";
import { Lead } from "src/lead/entity/lead.entity";
import { PromotionType } from "src/promotion-type/entity/promotion-type.entity";
import { PromoTemplate } from "src/promo-template/entity/promo-template.entity";
import { Contact } from "src/contacts/entity/contact.entity";
export declare class User {
    uuid: string;
    name: string;
    avatar: string;
    email: string;
    hashPassword(): void;
    password: string;
    business: Business;
    promotionType: PromotionType[];
    promoTemplates: PromoTemplate[];
    lead: Lead[];
    contacts: Contact[];
    role: UserRole;
    createdAt: string;
    updatedAt: string;
}
