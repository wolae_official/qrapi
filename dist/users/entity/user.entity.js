"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const openapi = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const crypto = __importStar(require("crypto"));
const business_entity_1 = require("../../business/entity/business.entity");
const user_role_enum_1 = require("../../core/enums/user-role.enum");
const lead_entity_1 = require("../../lead/entity/lead.entity");
const promotion_type_entity_1 = require("../../promotion-type/entity/promotion-type.entity");
const promo_template_entity_1 = require("../../promo-template/entity/promo-template.entity");
const contact_entity_1 = require("../../contacts/entity/contact.entity");
let User = class User {
    hashPassword() {
        this.password = crypto.createHmac("sha256", this.password).digest("hex");
    }
    static _OPENAPI_METADATA_FACTORY() {
        return { uuid: { required: true, type: () => String }, name: { required: true, type: () => String }, avatar: { required: true, type: () => String }, email: { required: true, type: () => String }, password: { required: true, type: () => String }, business: { required: true, type: () => require("../../business/entity/business.entity").Business }, promotionType: { required: true, type: () => [require("../../promotion-type/entity/promotion-type.entity").PromotionType] }, promoTemplates: { required: true, type: () => [require("../../promo-template/entity/promo-template.entity").PromoTemplate] }, lead: { required: true, type: () => [require("../../lead/entity/lead.entity").Lead] }, contacts: { required: true, type: () => [require("../../contacts/entity/contact.entity").Contact] }, role: { required: true, enum: require("../../core/enums/user-role.enum").UserRole }, createdAt: { required: true, type: () => String }, updatedAt: { required: true, type: () => String } };
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("uuid"),
    __metadata("design:type", String)
], User.prototype, "uuid", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: "" }),
    __metadata("design:type", String)
], User.prototype, "avatar", void 0);
__decorate([
    (0, typeorm_1.Column)({ unique: true }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.BeforeInsert)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], User.prototype, "hashPassword", null);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => business_entity_1.Business, (businees) => businees.employees, { cascade: ['insert', 'update'] }),
    __metadata("design:type", business_entity_1.Business)
], User.prototype, "business", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => promotion_type_entity_1.PromotionType, (promo) => promo.user, { cascade: ['insert', 'update'] }),
    (0, typeorm_1.JoinColumn)(),
    __metadata("design:type", Array)
], User.prototype, "promotionType", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => promo_template_entity_1.PromoTemplate, (promo) => promo.user, { cascade: ['insert', 'update'] }),
    (0, typeorm_1.JoinColumn)(),
    __metadata("design:type", Array)
], User.prototype, "promoTemplates", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => lead_entity_1.Lead, (lead) => lead.sender, { cascade: ['insert', 'update'] }),
    __metadata("design:type", Array)
], User.prototype, "lead", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => contact_entity_1.Contact, (contact) => contact.sender, { cascade: ['insert', 'update'] }),
    __metadata("design:type", Array)
], User.prototype, "contacts", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'enum', enum: user_role_enum_1.UserRole, default: user_role_enum_1.UserRole.User }),
    __metadata("design:type", String)
], User.prototype, "role", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", String)
], User.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", String)
], User.prototype, "updatedAt", void 0);
User = __decorate([
    (0, typeorm_1.Entity)()
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map