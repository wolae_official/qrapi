import { DeleteResponse } from 'src/core/models/delete-response.model';
import { CreateUserDto } from './../dto/users.dto';
import { User } from './../entity/user.entity';
import { UsersService } from './../service/users.service';
import { UserRole } from 'src/core/enums/user-role.enum';
export declare class UsersController {
    private usersService;
    constructor(usersService: UsersService);
    findAllUsers(): Promise<User[]>;
    findByEmail(email: string): Promise<User>;
    findById(id: string): Promise<User>;
    findByBusiness(businessId: string): Promise<User[]>;
    findByRole(role: UserRole): Promise<User[]>;
    create(dto: CreateUserDto): Promise<User>;
    update(id: string, dto: CreateUserDto): Promise<CreateUserDto>;
    delete(id: string): Promise<DeleteResponse>;
}
