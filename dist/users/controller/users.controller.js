"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const openapi = require("@nestjs/swagger");
const delete_response_model_1 = require("../../core/models/delete-response.model");
const users_dto_1 = require("./../dto/users.dto");
const users_service_1 = require("./../service/users.service");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const user_role_enum_1 = require("../../core/enums/user-role.enum");
const http_exception_filter_1 = require("../../core/filters/http-exception.filter");
let UsersController = class UsersController {
    constructor(usersService) {
        this.usersService = usersService;
    }
    findAllUsers() {
        return this.usersService.findAll();
    }
    findByEmail(email) {
        const userByemail = this.usersService.findByEmail(email);
        return userByemail.then(value => {
            if (!value)
                throw new common_1.HttpException('No se ha encontrado ningún usuario con ese email', common_1.HttpStatus.NOT_FOUND);
            return userByemail;
        });
    }
    findById(id) {
        const userById = this.usersService.findById(id);
        return userById.then(value => {
            if (!value)
                throw new common_1.HttpException('No se ha encontrado ningún usuario con ese ID', common_1.HttpStatus.NOT_FOUND);
            return userById;
        });
    }
    findByBusiness(businessId) {
        const userByBusiness = this.usersService.findByBusiness(businessId);
        return userByBusiness.then(value => {
            if (!value)
                throw new common_1.HttpException('No se ha encontrado ningún usuario asociado al negocio indicado', common_1.HttpStatus.NOT_FOUND);
            return userByBusiness;
        });
    }
    findByRole(role) {
        const userByRole = this.usersService.findByRole(role);
        return userByRole.then(value => {
            if (!value)
                throw new common_1.HttpException('No se ha encontrado ningún usuario con el rol indicado', common_1.HttpStatus.NOT_FOUND);
            return userByRole;
        });
    }
    create(dto) {
        return this.usersService.create(dto);
    }
    update(id, dto) {
        return this.usersService.update(id, dto);
    }
    delete(id) {
        const deletedUser = this.usersService.delete(id);
        return deletedUser.then(value => {
            if (value.affected === 0)
                throw new common_1.HttpException('No se ha podido borrar el lead correctamente', common_1.HttpStatus.BAD_REQUEST);
            return {
                status: common_1.HttpStatus.OK,
                message: `El lead con Id: ${id} ha sido eliminado correctamente`
            };
        });
    }
};
__decorate([
    (0, common_1.Get)(),
    openapi.ApiResponse({ status: 200, type: [require("../entity/user.entity").User] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "findAllUsers", null);
__decorate([
    (0, common_1.Get)('/email/:email'),
    openapi.ApiResponse({ status: 200, type: require("../entity/user.entity").User }),
    __param(0, (0, common_1.Param)('email')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "findByEmail", null);
__decorate([
    (0, common_1.Get)(':id'),
    openapi.ApiResponse({ status: 200, type: require("../entity/user.entity").User }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "findById", null);
__decorate([
    (0, common_1.Get)('/business/:business'),
    openapi.ApiResponse({ status: 200, type: [require("../entity/user.entity").User] }),
    __param(0, (0, common_1.Param)('business')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "findByBusiness", null);
__decorate([
    (0, common_1.Get)('/role/:role'),
    openapi.ApiResponse({ status: 200, type: [require("../entity/user.entity").User] }),
    __param(0, (0, common_1.Param)('role')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "findByRole", null);
__decorate([
    (0, common_1.Post)(),
    openapi.ApiResponse({ status: 201, type: require("../entity/user.entity").User }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [users_dto_1.CreateUserDto]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(':id'),
    openapi.ApiResponse({ status: 200, type: require("../dto/users.dto").CreateUserDto }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, users_dto_1.CreateUserDto]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    openapi.ApiResponse({ status: 200, type: require("../../core/models/delete-response.model").DeleteResponse }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "delete", null);
UsersController = __decorate([
    (0, swagger_1.ApiTags)('Users'),
    (0, common_1.Controller)('users'),
    (0, common_1.UseFilters)(http_exception_filter_1.HttpExceptionFilter),
    __metadata("design:paramtypes", [users_service_1.UsersService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map