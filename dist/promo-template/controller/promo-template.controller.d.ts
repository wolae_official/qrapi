import { DeleteResponse } from 'src/core/models/delete-response.model';
import { PromoTemplateDto } from '../dto/promo-template.dto';
import { PromoTemplate } from '../entity/promo-template.entity';
import { PromoTemplateService } from '../service/promo-template.service';
export declare class PromoTemplateController {
    private ptService;
    constructor(ptService: PromoTemplateService);
    findAll(): Promise<PromoTemplate[]>;
    findByUuid(uuid: string): Promise<PromoTemplate>;
    create(dto: PromoTemplateDto): Promise<PromoTemplate>;
    update(uuid: string, dto: PromoTemplateDto): Promise<PromoTemplate>;
    delete(uuid: string): Promise<DeleteResponse>;
}
