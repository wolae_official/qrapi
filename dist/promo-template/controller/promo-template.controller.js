"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromoTemplateController = void 0;
const openapi = require("@nestjs/swagger");
const delete_response_model_1 = require("../../core/models/delete-response.model");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const promo_template_dto_1 = require("../dto/promo-template.dto");
const promo_template_service_1 = require("../service/promo-template.service");
const http_exception_filter_1 = require("../../core/filters/http-exception.filter");
let PromoTemplateController = class PromoTemplateController {
    constructor(ptService) {
        this.ptService = ptService;
    }
    findAll() {
        return this.ptService.findAll();
    }
    findByUuid(uuid) {
        const ptById = this.ptService.findOne(uuid);
        return ptById.then(value => {
            if (!value)
                throw new common_1.HttpException('No se ha encontrado ninguna promo template con ese ID', common_1.HttpStatus.NOT_FOUND);
            return ptById;
        });
    }
    create(dto) {
        return this.ptService.create(dto);
    }
    update(uuid, dto) {
        return this.ptService.update(uuid, dto);
    }
    delete(uuid) {
        const deletedPromoTemplate = this.ptService.remove(uuid);
        return deletedPromoTemplate.then(value => {
            if (value.affected === 0)
                throw new common_1.HttpException('No se ha podido borrar el promo template correctamente', common_1.HttpStatus.BAD_REQUEST);
            return {
                status: common_1.HttpStatus.OK,
                message: `El promo template con Id: ${uuid} ha sido eliminado correctamente`
            };
        });
    }
};
__decorate([
    (0, common_1.Get)(),
    openapi.ApiResponse({ status: 200, type: [require("../entity/promo-template.entity").PromoTemplate] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PromoTemplateController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':uuid'),
    openapi.ApiResponse({ status: 200, type: require("../entity/promo-template.entity").PromoTemplate }),
    __param(0, (0, common_1.Param)('uuid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PromoTemplateController.prototype, "findByUuid", null);
__decorate([
    (0, common_1.Post)(),
    openapi.ApiResponse({ status: 201, type: require("../entity/promo-template.entity").PromoTemplate }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [promo_template_dto_1.PromoTemplateDto]),
    __metadata("design:returntype", Promise)
], PromoTemplateController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    openapi.ApiResponse({ status: 200, type: require("../entity/promo-template.entity").PromoTemplate }),
    __param(0, (0, common_1.Param)('uuid')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, promo_template_dto_1.PromoTemplateDto]),
    __metadata("design:returntype", Promise)
], PromoTemplateController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':uuid'),
    openapi.ApiResponse({ status: 200, type: require("../../core/models/delete-response.model").DeleteResponse }),
    __param(0, (0, common_1.Param)('uuid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PromoTemplateController.prototype, "delete", null);
PromoTemplateController = __decorate([
    (0, swagger_1.ApiTags)('Promo Template'),
    (0, common_1.Controller)('promotemplate'),
    (0, common_1.UseFilters)(http_exception_filter_1.HttpExceptionFilter),
    __metadata("design:paramtypes", [promo_template_service_1.PromoTemplateService])
], PromoTemplateController);
exports.PromoTemplateController = PromoTemplateController;
//# sourceMappingURL=promo-template.controller.js.map