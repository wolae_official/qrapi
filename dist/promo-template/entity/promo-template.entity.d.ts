import { Lead } from 'src/lead/entity/lead.entity';
import { PromotionType } from 'src/promotion-type/entity/promotion-type.entity';
import { User } from 'src/users/entity/user.entity';
export declare class PromoTemplate {
    uuid: string;
    user: User;
    promotionType: PromotionType;
    lead: Lead;
    title: string;
    description: string;
    expires: string;
    uses: number;
    availability: number;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}
