"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromoTemplate = void 0;
const openapi = require("@nestjs/swagger");
const lead_entity_1 = require("../../lead/entity/lead.entity");
const promotion_type_entity_1 = require("../../promotion-type/entity/promotion-type.entity");
const user_entity_1 = require("../../users/entity/user.entity");
const typeorm_1 = require("typeorm");
let PromoTemplate = class PromoTemplate {
    static _OPENAPI_METADATA_FACTORY() {
        return { uuid: { required: true, type: () => String }, user: { required: true, type: () => require("../../users/entity/user.entity").User }, promotionType: { required: true, type: () => require("../../promotion-type/entity/promotion-type.entity").PromotionType }, lead: { required: true, type: () => require("../../lead/entity/lead.entity").Lead }, title: { required: true, type: () => String }, description: { required: true, type: () => String }, expires: { required: true, type: () => String }, uses: { required: true, type: () => Number }, availability: { required: true, type: () => Number }, isActive: { required: true, type: () => Boolean }, createdAt: { required: true, type: () => String }, updatedAt: { required: true, type: () => String } };
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("uuid"),
    __metadata("design:type", String)
], PromoTemplate.prototype, "uuid", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => user_entity_1.User, (user) => user.promoTemplates),
    __metadata("design:type", user_entity_1.User)
], PromoTemplate.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => promotion_type_entity_1.PromotionType, (promoType) => promoType.promoTemplate),
    __metadata("design:type", promotion_type_entity_1.PromotionType)
], PromoTemplate.prototype, "promotionType", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(type => lead_entity_1.Lead, (lead) => lead.promotemplate),
    __metadata("design:type", lead_entity_1.Lead)
], PromoTemplate.prototype, "lead", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PromoTemplate.prototype, "title", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PromoTemplate.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PromoTemplate.prototype, "expires", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PromoTemplate.prototype, "uses", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PromoTemplate.prototype, "availability", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Boolean)
], PromoTemplate.prototype, "isActive", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", String)
], PromoTemplate.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", String)
], PromoTemplate.prototype, "updatedAt", void 0);
PromoTemplate = __decorate([
    (0, typeorm_1.Entity)()
], PromoTemplate);
exports.PromoTemplate = PromoTemplate;
//# sourceMappingURL=promo-template.entity.js.map