"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromoTemplateDto = void 0;
const openapi = require("@nestjs/swagger");
const promotion_type_entity_1 = require("../../promotion-type/entity/promotion-type.entity");
const user_entity_1 = require("../../users/entity/user.entity");
class PromoTemplateDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { user: { required: true, type: () => require("../../users/entity/user.entity").User }, promotionType: { required: true, type: () => require("../../promotion-type/entity/promotion-type.entity").PromotionType }, title: { required: true, type: () => String }, description: { required: true, type: () => String }, expires: { required: true, type: () => String }, uses: { required: true, type: () => Number }, availability: { required: true, type: () => Number }, isActive: { required: true, type: () => Boolean } };
    }
}
exports.PromoTemplateDto = PromoTemplateDto;
//# sourceMappingURL=promo-template.dto.js.map