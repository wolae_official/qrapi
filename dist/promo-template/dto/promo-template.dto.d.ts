import { PromotionType } from "src/promotion-type/entity/promotion-type.entity";
import { User } from "src/users/entity/user.entity";
export declare class PromoTemplateDto {
    user: User;
    promotionType: PromotionType;
    title: string;
    description: string;
    expires: string;
    uses: number;
    availability: number;
    isActive: boolean;
}
