import { DeleteResult, Repository } from 'typeorm';
import { PromoTemplateDto } from '../dto/promo-template.dto';
import { PromoTemplate } from '../entity/promo-template.entity';
export declare class PromoTemplateService {
    private promoTemplateRepository;
    constructor(promoTemplateRepository: Repository<PromoTemplate>);
    findAll(): Promise<PromoTemplate[]>;
    findOne(uuid: string): Promise<PromoTemplate>;
    create(dto: PromoTemplateDto): Promise<PromoTemplate>;
    update(uuid: string, dto: PromoTemplateDto): Promise<PromoTemplate>;
    remove(uuid: string): Promise<DeleteResult>;
}
