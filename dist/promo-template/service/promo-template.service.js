"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromoTemplateService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const promo_template_entity_1 = require("../entity/promo-template.entity");
let PromoTemplateService = class PromoTemplateService {
    constructor(promoTemplateRepository) {
        this.promoTemplateRepository = promoTemplateRepository;
    }
    async findAll() {
        return await this.promoTemplateRepository.find({ relations: ['user', 'promotionType', 'lead'] });
    }
    async findOne(uuid) {
        return await this.promoTemplateRepository.findOne(uuid, { relations: ['user', 'promotionType', 'lead'] });
    }
    async create(dto) {
        return await this.promoTemplateRepository.save(dto);
    }
    async update(uuid, dto) {
        const qr = await this.findOne(uuid);
        const editedQr = Object.assign(qr, dto);
        return await this.promoTemplateRepository.save(editedQr);
    }
    async remove(uuid) {
        return await this.promoTemplateRepository.delete(uuid);
    }
};
PromoTemplateService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(promo_template_entity_1.PromoTemplate)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], PromoTemplateService);
exports.PromoTemplateService = PromoTemplateService;
//# sourceMappingURL=promo-template.service.js.map