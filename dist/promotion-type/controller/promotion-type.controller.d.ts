import { DeleteResponse } from 'src/core/models/delete-response.model';
import { PromotionTypeDto } from '../dto/promotion-type.dto';
import { PromotionType } from '../entity/promotion-type.entity';
import { PromotionTypeService } from '../service/promotion-type.service';
export declare class PromotionTypeController {
    private promotionTypeService;
    constructor(promotionTypeService: PromotionTypeService);
    findAllPromotionType(): Promise<PromotionType[]>;
    findByUuid(uuid: string): Promise<PromotionType>;
    create(dto: PromotionTypeDto): Promise<PromotionType>;
    update(uuid: string, dto: PromotionTypeDto): Promise<PromotionTypeDto>;
    delete(uuid: string): Promise<DeleteResponse>;
}
