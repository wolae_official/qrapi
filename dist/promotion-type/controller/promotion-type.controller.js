"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromotionTypeController = void 0;
const openapi = require("@nestjs/swagger");
const delete_response_model_1 = require("../../core/models/delete-response.model");
const promotion_type_dto_1 = require("../dto/promotion-type.dto");
const promotion_type_service_1 = require("../service/promotion-type.service");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const http_exception_filter_1 = require("../../core/filters/http-exception.filter");
let PromotionTypeController = class PromotionTypeController {
    constructor(promotionTypeService) {
        this.promotionTypeService = promotionTypeService;
    }
    findAllPromotionType() {
        return this.promotionTypeService.findAll();
    }
    findByUuid(uuid) {
        const promotionTypeById = this.promotionTypeService.findOne(uuid);
        return promotionTypeById.then(value => {
            if (!value)
                throw new common_1.HttpException('No se ha encontrado ningún tipo de promoción con ese ID', common_1.HttpStatus.NOT_FOUND);
            return promotionTypeById;
        });
    }
    create(dto) {
        return this.promotionTypeService.create(dto);
    }
    update(uuid, dto) {
        return this.promotionTypeService.update(uuid, dto);
    }
    delete(uuid) {
        const deletedPromotionType = this.promotionTypeService.remove(uuid);
        return deletedPromotionType.then(value => {
            if (value.affected === 0)
                throw new common_1.HttpException('No se ha podido borrar el lead correctamente', common_1.HttpStatus.BAD_REQUEST);
            return {
                status: common_1.HttpStatus.OK,
                message: `El lead con Id: ${uuid} ha sido eliminado correctamente`
            };
        });
    }
};
__decorate([
    (0, common_1.Get)(),
    openapi.ApiResponse({ status: 200, type: [require("../entity/promotion-type.entity").PromotionType] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PromotionTypeController.prototype, "findAllPromotionType", null);
__decorate([
    (0, common_1.Get)(':uuid'),
    openapi.ApiResponse({ status: 200, type: require("../entity/promotion-type.entity").PromotionType }),
    __param(0, (0, common_1.Param)('uuid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PromotionTypeController.prototype, "findByUuid", null);
__decorate([
    (0, common_1.Post)(),
    openapi.ApiResponse({ status: 201, type: require("../entity/promotion-type.entity").PromotionType }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [promotion_type_dto_1.PromotionTypeDto]),
    __metadata("design:returntype", void 0)
], PromotionTypeController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(':uuid'),
    openapi.ApiResponse({ status: 200, type: require("../dto/promotion-type.dto").PromotionTypeDto }),
    __param(0, (0, common_1.Param)('uuid')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, promotion_type_dto_1.PromotionTypeDto]),
    __metadata("design:returntype", void 0)
], PromotionTypeController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':uuid'),
    openapi.ApiResponse({ status: 200, type: require("../../core/models/delete-response.model").DeleteResponse }),
    __param(0, (0, common_1.Param)('uuid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PromotionTypeController.prototype, "delete", null);
PromotionTypeController = __decorate([
    (0, swagger_1.ApiTags)('PromotionType'),
    (0, common_1.Controller)('promotion-type'),
    (0, common_1.UseFilters)(http_exception_filter_1.HttpExceptionFilter),
    __metadata("design:paramtypes", [promotion_type_service_1.PromotionTypeService])
], PromotionTypeController);
exports.PromotionTypeController = PromotionTypeController;
//# sourceMappingURL=promotion-type.controller.js.map