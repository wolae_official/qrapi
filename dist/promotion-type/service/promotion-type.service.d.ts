import { PromotionTypeDto } from '../dto/promotion-type.dto';
import { DeleteResult, Repository } from 'typeorm';
import { PromotionType } from '../entity/promotion-type.entity';
export declare class PromotionTypeService {
    private PromotionTypeRepository;
    constructor(PromotionTypeRepository: Repository<PromotionType>);
    findAll(): Promise<PromotionType[]>;
    findOne(uuid: string): Promise<PromotionType>;
    create(dto: PromotionTypeDto): Promise<PromotionType>;
    update(uuid: string, dto: PromotionTypeDto): Promise<PromotionTypeDto>;
    remove(uuid: string): Promise<DeleteResult>;
}
