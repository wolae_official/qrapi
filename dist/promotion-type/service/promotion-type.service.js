"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromotionTypeService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const promotion_type_entity_1 = require("../entity/promotion-type.entity");
let PromotionTypeService = class PromotionTypeService {
    constructor(PromotionTypeRepository) {
        this.PromotionTypeRepository = PromotionTypeRepository;
    }
    async findAll() {
        return await this.PromotionTypeRepository.find({ relations: ['user'] });
    }
    async findOne(uuid) {
        return await this.PromotionTypeRepository.findOne(uuid, { relations: ['user'] });
    }
    async create(dto) {
        return await this.PromotionTypeRepository.save(dto);
    }
    async update(uuid, dto) {
        const promotionType = await this.findOne(uuid);
        const editedPromotionType = Object.assign(promotionType, dto);
        return await this.PromotionTypeRepository.save(editedPromotionType);
    }
    async remove(uuid) {
        return await this.PromotionTypeRepository.delete(uuid);
    }
};
PromotionTypeService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(promotion_type_entity_1.PromotionType)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], PromotionTypeService);
exports.PromotionTypeService = PromotionTypeService;
//# sourceMappingURL=promotion-type.service.js.map