import { User } from "src/users/entity/user.entity";
export declare class PromotionTypeDto {
    label: string;
    user: User;
    status: boolean;
}
