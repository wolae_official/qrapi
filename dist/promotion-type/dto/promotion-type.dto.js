"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromotionTypeDto = void 0;
const openapi = require("@nestjs/swagger");
const user_entity_1 = require("../../users/entity/user.entity");
class PromotionTypeDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { label: { required: true, type: () => String }, user: { required: true, type: () => require("../../users/entity/user.entity").User }, status: { required: true, type: () => Boolean } };
    }
}
exports.PromotionTypeDto = PromotionTypeDto;
//# sourceMappingURL=promotion-type.dto.js.map