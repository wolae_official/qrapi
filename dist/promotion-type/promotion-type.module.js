"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromotionTypeModule = void 0;
const promotion_type_service_1 = require("./service/promotion-type.service");
const promotion_type_entity_1 = require("./entity/promotion-type.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const promotion_type_controller_1 = require("./controller/promotion-type.controller");
const user_entity_1 = require("../users/entity/user.entity");
let PromotionTypeModule = class PromotionTypeModule {
};
PromotionTypeModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([promotion_type_entity_1.PromotionType, user_entity_1.User])],
        controllers: [promotion_type_controller_1.PromotionTypeController],
        providers: [promotion_type_service_1.PromotionTypeService]
    })
], PromotionTypeModule);
exports.PromotionTypeModule = PromotionTypeModule;
//# sourceMappingURL=promotion-type.module.js.map