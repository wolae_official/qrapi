import { PromoTemplate } from 'src/promo-template/entity/promo-template.entity';
import { User } from 'src/users/entity/user.entity';
export declare class PromotionType {
    uuid: string;
    label: string;
    user: User;
    promoTemplate: PromoTemplate[];
    status: boolean;
    createdAt: string;
    updatedAt: string;
}
