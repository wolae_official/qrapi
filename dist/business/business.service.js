"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessService = void 0;
const business_entity_1 = require("./entity/business.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
let BusinessService = class BusinessService {
    constructor(businessRepository) {
        this.businessRepository = businessRepository;
    }
    async findAll() {
        return await this.businessRepository.find({ relations: ['employees'] });
    }
    async findOne(id) {
        return await this.businessRepository.findOne(id, { relations: ['employees'] });
    }
    async create(businessDto) {
        return await this.businessRepository.save(businessDto);
    }
    async update(id, updateBusinessDto) {
        const business = await this.findOne(id);
        const editedBusiness = Object.assign(business, updateBusinessDto);
        return await this.businessRepository.save(editedBusiness);
    }
    async remove(id) {
        return await this.businessRepository.delete(id);
    }
};
BusinessService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(business_entity_1.Business)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], BusinessService);
exports.BusinessService = BusinessService;
//# sourceMappingURL=business.service.js.map