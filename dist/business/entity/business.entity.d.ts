import { User } from 'src/users/entity/user.entity';
export declare class Business {
    uuid: string;
    name: string;
    location: string;
    image: string;
    status: boolean;
    employees: User[];
    createdAt: string;
    updatedAt: string;
}
