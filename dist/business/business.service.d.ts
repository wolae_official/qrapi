import { Business } from './entity/business.entity';
import { DeleteResult, Repository } from 'typeorm';
import { BusinessDto } from './dto/business.dto';
export declare class BusinessService {
    private businessRepository;
    constructor(businessRepository: Repository<Business>);
    findAll(): Promise<Business[]>;
    findOne(id: string): Promise<Business>;
    create(businessDto: BusinessDto): Promise<Business>;
    update(id: string, updateBusinessDto: BusinessDto): Promise<Business>;
    remove(id: string): Promise<DeleteResult>;
}
