"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessDto = void 0;
const openapi = require("@nestjs/swagger");
const user_entity_1 = require("../../users/entity/user.entity");
class BusinessDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { name: { required: true, type: () => String }, employees: { required: true, type: () => [require("../../users/entity/user.entity").User] }, status: { required: true, type: () => Boolean }, location: { required: false, type: () => String }, image: { required: false, type: () => String } };
    }
}
exports.BusinessDto = BusinessDto;
//# sourceMappingURL=business.dto.js.map