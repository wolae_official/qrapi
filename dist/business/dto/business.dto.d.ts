import { User } from 'src/users/entity/user.entity';
export declare class BusinessDto {
    name: string;
    employees: User[];
    status: boolean;
    location?: string;
    image?: string;
}
