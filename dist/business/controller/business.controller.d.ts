import { DeleteResponse } from 'src/core/models/delete-response.model';
import { BusinessService } from '../business.service';
import { BusinessDto } from '../dto/business.dto';
import { Business } from '../entity/business.entity';
export declare class BusinessController {
    private readonly businessService;
    constructor(businessService: BusinessService);
    findAll(): Promise<Business[]>;
    findOne(id: string): Promise<Business>;
    create(businessDto: BusinessDto): Promise<Business>;
    update(id: string, updateBusinessDto: BusinessDto): Promise<Business>;
    remove(id: string): Promise<DeleteResponse>;
}
