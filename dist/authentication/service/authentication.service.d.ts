import { JwtService } from "@nestjs/jwt";
import { RegisterUserDto } from "src/users/dto/users.dto";
import { User } from "src/users/entity/user.entity";
import { UsersService } from "src/users/service/users.service";
import LoginDto from "../dto/login.dto";
import { AccessToken } from 'src/core/models/access-token.model';
export declare class AuthenticationService {
    private readonly userService;
    private readonly jwtService;
    constructor(userService: UsersService, jwtService: JwtService);
    private validate;
    login(user: LoginDto): Promise<AccessToken | {
        status: number;
    }>;
    register(user: RegisterUserDto): Promise<User>;
    private setExpireDate;
}
