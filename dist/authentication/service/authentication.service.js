"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const users_dto_1 = require("../../users/dto/users.dto");
const user_entity_1 = require("../../users/entity/user.entity");
const users_service_1 = require("../../users/service/users.service");
const crypto = __importStar(require("crypto"));
const access_token_model_1 = require("../../core/models/access-token.model");
let AuthenticationService = class AuthenticationService {
    constructor(userService, jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
    }
    async validate(userData) {
        return await this.userService.findByEmail(userData.email);
    }
    async login(user) {
        const hashedPassword = crypto
            .createHmac("sha256", user.password)
            .digest("hex");
        return await this.validate(user).then((userData) => {
            if (!userData || hashedPassword !== userData.password) {
                return { status: 404 };
            }
            const payload = {
                id: `${userData.uuid}`,
                exp: this.setExpireDate(),
            }, accessToken = this.jwtService.sign(payload);
            return {
                expires_in: 3600,
                access_token: accessToken,
                payload,
                status: 200,
            };
        });
    }
    async register(user) {
        return await this.userService.create(user);
    }
    setExpireDate() {
        const creationDate = new Date();
        return creationDate.setHours(creationDate.getHours() + 1);
    }
};
AuthenticationService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        jwt_1.JwtService])
], AuthenticationService);
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map