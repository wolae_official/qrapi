import { UsersService } from 'src/users/service/users.service';
import { RegisterUserDto } from "src/users/dto/users.dto";
import LoginDto from "../dto/login.dto";
import { AuthenticationService } from "../service/authentication.service";
import { User } from './../../users/entity/user.entity';
import { AccessToken } from 'src/core/models/access-token.model';
export declare class AuthenticationController {
    private readonly authService;
    private usersService;
    constructor(authService: AuthenticationService, usersService: UsersService);
    login(user: LoginDto): Promise<AccessToken | {
        status: number;
    }>;
    register(user: RegisterUserDto): Promise<User>;
}
