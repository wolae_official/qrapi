"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationController = void 0;
const openapi = require("@nestjs/swagger");
const users_service_1 = require("../../users/service/users.service");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const users_dto_1 = require("../../users/dto/users.dto");
const login_dto_1 = __importDefault(require("../dto/login.dto"));
const authentication_service_1 = require("../service/authentication.service");
const http_exception_filter_1 = require("../../core/filters/http-exception.filter");
const access_token_model_1 = require("../../core/models/access-token.model");
let AuthenticationController = class AuthenticationController {
    constructor(authService, usersService) {
        this.authService = authService;
        this.usersService = usersService;
    }
    login(user) {
        const login = this.authService.login(user);
        return login.then(value => {
            if (value.status === 404)
                throw new common_1.HttpException('Las credenciales introducidas no son correctas', common_1.HttpStatus.NOT_FOUND);
            return login;
        });
    }
    register(user) {
        const existingUser = this.usersService.findByEmail(user.email);
        return existingUser.then(value => {
            if (value)
                throw new common_1.HttpException('Ya existe un usuario con ese email', common_1.HttpStatus.BAD_REQUEST);
            return this.authService.register(user);
        });
    }
};
__decorate([
    (0, common_1.Post)("login"),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_dto_1.default]),
    __metadata("design:returntype", Promise)
], AuthenticationController.prototype, "login", null);
__decorate([
    (0, common_1.Post)("register"),
    openapi.ApiResponse({ status: 201, type: require("../../users/entity/user.entity").User }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [users_dto_1.RegisterUserDto]),
    __metadata("design:returntype", Promise)
], AuthenticationController.prototype, "register", null);
AuthenticationController = __decorate([
    (0, swagger_1.ApiTags)('Auth'),
    (0, common_1.Controller)("authentication"),
    (0, common_1.UseFilters)(http_exception_filter_1.HttpExceptionFilter),
    __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
        users_service_1.UsersService])
], AuthenticationController);
exports.AuthenticationController = AuthenticationController;
//# sourceMappingURL=authentication.controller.js.map