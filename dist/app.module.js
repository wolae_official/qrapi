"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const promo_template_module_1 = require("./promo-template/promo-template.module");
const promotion_type_module_1 = require("./promotion-type/promotion-type.module");
const users_module_1 = require("./users/users.module");
const authentication_module_1 = require("./authentication/authentication.module");
const business_module_1 = require("./business/business.module");
const lead_module_1 = require("./lead/lead.module");
const qr_generator_module_1 = require("./qr-generator/qr-generator.module");
const contacts_module_1 = require("./contacts/contacts.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forRoot(),
            promo_template_module_1.PromoTemplateModule,
            promotion_type_module_1.PromotionTypeModule,
            users_module_1.UsersModule,
            authentication_module_1.AuthenticationModule,
            business_module_1.BusinessModule,
            lead_module_1.LeadModule,
            qr_generator_module_1.QrGeneratorModule,
            contacts_module_1.ContactsModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map