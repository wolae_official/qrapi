import { HttpStatus } from '@nestjs/common';
export declare class DeleteResponse {
    status: HttpStatus;
    message: string;
}
