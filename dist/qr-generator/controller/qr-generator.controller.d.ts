import { QrGeneratorDto } from '../dto/qr-generator.dto';
import { QrGeneratorService } from '../service/qr-generator.service';
export declare class QrGeneratorController {
    private qrGeneratorService;
    constructor(qrGeneratorService: QrGeneratorService);
    generateQR(qr: QrGeneratorDto): Promise<string>;
}
