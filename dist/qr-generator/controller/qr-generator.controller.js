"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QrGeneratorController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const qr_generator_dto_1 = require("../dto/qr-generator.dto");
const qr_generator_service_1 = require("../service/qr-generator.service");
let QrGeneratorController = class QrGeneratorController {
    constructor(qrGeneratorService) {
        this.qrGeneratorService = qrGeneratorService;
    }
    generateQR(qr) {
        return this.qrGeneratorService.generateQR(qr.uuid);
    }
};
__decorate([
    (0, common_1.Post)(),
    openapi.ApiResponse({ status: 201, type: String }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [qr_generator_dto_1.QrGeneratorDto]),
    __metadata("design:returntype", void 0)
], QrGeneratorController.prototype, "generateQR", null);
QrGeneratorController = __decorate([
    (0, swagger_1.ApiTags)('Qr Generator'),
    (0, common_1.Controller)('qr-generator'),
    __metadata("design:paramtypes", [qr_generator_service_1.QrGeneratorService])
], QrGeneratorController);
exports.QrGeneratorController = QrGeneratorController;
//# sourceMappingURL=qr-generator.controller.js.map