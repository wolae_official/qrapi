"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QrGeneratorModule = void 0;
const common_1 = require("@nestjs/common");
const qr_generator_service_1 = require("./service/qr-generator.service");
const qr_generator_controller_1 = require("./controller/qr-generator.controller");
const lead_module_1 = require("../lead/lead.module");
let QrGeneratorModule = class QrGeneratorModule {
};
QrGeneratorModule = __decorate([
    (0, common_1.Module)({
        imports: [lead_module_1.LeadModule],
        providers: [qr_generator_service_1.QrGeneratorService],
        controllers: [qr_generator_controller_1.QrGeneratorController]
    })
], QrGeneratorModule);
exports.QrGeneratorModule = QrGeneratorModule;
//# sourceMappingURL=qr-generator.module.js.map