"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QrGeneratorDto = void 0;
const openapi = require("@nestjs/swagger");
class QrGeneratorDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { uuid: { required: true, type: () => String } };
    }
}
exports.QrGeneratorDto = QrGeneratorDto;
//# sourceMappingURL=qr-generator.dto.js.map