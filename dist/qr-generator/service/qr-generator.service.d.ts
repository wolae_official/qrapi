import { LeadService } from 'src/lead/service/lead.service';
export declare class QrGeneratorService {
    private leadService;
    constructor(leadService: LeadService);
    generateQR(uuid: string): Promise<string>;
    private generateOptions;
}
