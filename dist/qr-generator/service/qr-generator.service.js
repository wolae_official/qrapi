"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QrGeneratorService = void 0;
const common_1 = require("@nestjs/common");
const lead_entity_1 = require("../../lead/entity/lead.entity");
const lead_service_1 = require("../../lead/service/lead.service");
const QRCode = require("easyqrcodejs-nodejs");
let QrGeneratorService = class QrGeneratorService {
    constructor(leadService) {
        this.leadService = leadService;
    }
    async generateQR(uuid) {
        let leadToGenerate;
        await this.leadService.findOne(uuid).then((lead) => {
            leadToGenerate = lead;
        });
        const qrcode = new QRCode(this.generateOptions(leadToGenerate.uuid));
        qrcode.saveImage({
            path: 'src/qr-generated_images/myQr_1.png'
        });
        return '';
    }
    generateOptions(uuid) {
        const options = {
            text: uuid,
            width: 256,
            height: 256,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H,
            backgroundImage: 'src/wolae_icon_logo.jpeg',
            backgroundImageAlpha: 1,
            autoColor: false,
            autoColorDark: "rgba(0, 0, 0, .6)",
            autoColorLight: "rgba(255, 255, 255, .7)",
        };
        return options;
    }
};
QrGeneratorService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [lead_service_1.LeadService])
], QrGeneratorService);
exports.QrGeneratorService = QrGeneratorService;
//# sourceMappingURL=qr-generator.service.js.map