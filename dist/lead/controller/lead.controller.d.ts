import { DeleteResponse } from 'src/core/models/delete-response.model';
import { LeadDto } from '../dto/lead.dto';
import { Lead } from '../entity/lead.entity';
import { LeadService } from '../service/lead.service';
export declare class LeadController {
    private leadService;
    constructor(leadService: LeadService);
    findAll(): Promise<Lead[]>;
    findByUuid(uuid: string): Promise<Lead>;
    create(dto: LeadDto): Promise<Lead>;
    update(uuid: string, dto: LeadDto): Promise<Lead>;
    delete(uuid: string): Promise<DeleteResponse>;
}
