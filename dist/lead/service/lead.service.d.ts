import { Contact } from 'src/contacts/entity/contact.entity';
import { Repository, DeleteResult } from 'typeorm';
import { LeadDto } from '../dto/lead.dto';
import { Lead } from '../entity/lead.entity';
export declare class LeadService {
    private leadRepository;
    private contactRepository;
    constructor(leadRepository: Repository<Lead>, contactRepository: Repository<Contact>);
    findAll(): Promise<Lead[]>;
    findOne(uuid: string): Promise<Lead>;
    create(dto: LeadDto): Promise<Lead>;
    update(uuid: string, dto: LeadDto): Promise<Lead>;
    remove(uuid: string): Promise<DeleteResult>;
}
