"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeadService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const contact_entity_1 = require("../../contacts/entity/contact.entity");
const typeorm_2 = require("typeorm");
const lead_entity_1 = require("../entity/lead.entity");
let LeadService = class LeadService {
    constructor(leadRepository, contactRepository) {
        this.leadRepository = leadRepository;
        this.contactRepository = contactRepository;
    }
    async findAll() {
        return await this.leadRepository.find({ relations: ['contact', 'promotemplate', 'sender'] });
    }
    async findOne(uuid) {
        return await this.leadRepository.findOne(uuid, { relations: ['contact', 'promotemplate', 'sender'] });
    }
    async create(dto) {
        this.contactRepository.save(dto.contact);
        return await this.leadRepository.save(dto);
    }
    async update(uuid, dto) {
        const qr = await this.findOne(uuid);
        const editedQr = Object.assign(qr, dto);
        return await this.leadRepository.save(editedQr);
    }
    async remove(uuid) {
        return await this.leadRepository.delete(uuid);
    }
};
LeadService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(lead_entity_1.Lead)),
    __param(1, (0, typeorm_1.InjectRepository)(lead_entity_1.Lead)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], LeadService);
exports.LeadService = LeadService;
//# sourceMappingURL=lead.service.js.map