import { Contact } from 'src/contacts/entity/contact.entity';
import { PromoTemplate } from 'src/promo-template/entity/promo-template.entity';
import { User } from 'src/users/entity/user.entity';
export declare class Lead {
    uuid: string;
    sender: User;
    contact: Contact;
    promotemplate: PromoTemplate;
    uses_availables: number;
    isActive: boolean;
    createdAt: string;
    updatedAt: string;
}
