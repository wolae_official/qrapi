"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Lead = void 0;
const openapi = require("@nestjs/swagger");
const contact_entity_1 = require("../../contacts/entity/contact.entity");
const promo_template_entity_1 = require("../../promo-template/entity/promo-template.entity");
const user_entity_1 = require("../../users/entity/user.entity");
const typeorm_1 = require("typeorm");
let Lead = class Lead {
    static _OPENAPI_METADATA_FACTORY() {
        return { uuid: { required: true, type: () => String }, sender: { required: true, type: () => require("../../users/entity/user.entity").User }, contact: { required: true, type: () => require("../../contacts/entity/contact.entity").Contact }, promotemplate: { required: true, type: () => require("../../promo-template/entity/promo-template.entity").PromoTemplate }, uses_availables: { required: true, type: () => Number }, isActive: { required: true, type: () => Boolean }, createdAt: { required: true, type: () => String }, updatedAt: { required: true, type: () => String } };
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("uuid"),
    __metadata("design:type", String)
], Lead.prototype, "uuid", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => user_entity_1.User, (user) => user.lead),
    __metadata("design:type", user_entity_1.User)
], Lead.prototype, "sender", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => contact_entity_1.Contact, (contact) => contact.lead, { cascade: ['insert', 'update'] }),
    __metadata("design:type", contact_entity_1.Contact)
], Lead.prototype, "contact", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(type => promo_template_entity_1.PromoTemplate, (ptemplate) => ptemplate.lead),
    (0, typeorm_1.JoinColumn)(),
    __metadata("design:type", promo_template_entity_1.PromoTemplate)
], Lead.prototype, "promotemplate", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], Lead.prototype, "uses_availables", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Boolean)
], Lead.prototype, "isActive", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", String)
], Lead.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", String)
], Lead.prototype, "updatedAt", void 0);
Lead = __decorate([
    (0, typeorm_1.Entity)()
], Lead);
exports.Lead = Lead;
//# sourceMappingURL=lead.entity.js.map