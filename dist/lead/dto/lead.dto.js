"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeadDto = void 0;
const openapi = require("@nestjs/swagger");
const contact_entity_1 = require("../../contacts/entity/contact.entity");
const promo_template_entity_1 = require("../../promo-template/entity/promo-template.entity");
const user_entity_1 = require("../../users/entity/user.entity");
class LeadDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { sender: { required: true, type: () => require("../../users/entity/user.entity").User }, contact: { required: true, type: () => require("../../contacts/entity/contact.entity").Contact }, promotemplate: { required: true, type: () => require("../../promo-template/entity/promo-template.entity").PromoTemplate }, uses_availables: { required: true, type: () => Number }, isActive: { required: true, type: () => Boolean } };
    }
}
exports.LeadDto = LeadDto;
//# sourceMappingURL=lead.dto.js.map